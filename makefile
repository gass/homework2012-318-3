all: main.o slacks.o
	mkdir -p build 
	g++ $^ -o build/all


slacks.o: slacks.cpp slacks.h
	g++ -c slacks.cpp 

main.o: main.cpp
	g++ -c main.cpp

.PHONY:  
	clean

clean:
	rm  -rf main.o slacks.o

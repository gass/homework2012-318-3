#include "slacks.h"

using namespace std;

Edge::Edge() {
    vertex = -1;
    delay = 0;
}

Edge::Edge(int _vertex, int _delay) {
    vertex=_vertex;
    delay=_delay;
}

Circuit::Circuit(const char *file_nodes, 
                 const char *file_nets, 
                 const char *file_aux) {
    fstream nodes, nets, aux;
    int temp1, temp2;
    double temp3;
    Edge tmp_edge; 
    nodes.open(file_nodes, fstream::in);
    if (!nodes) {
        throw DontOpenFileException(file_nodes);
    }
    nets.open(file_nets, fstream::in);
    if (!nets) {
        throw DontOpenFileException(file_nets);
    }
    aux.open(file_aux, fstream::in);
    if (!aux) {
        throw DontOpenFileException(file_aux);
    }
    while (1) {
        nodes>>temp3;
        if(nodes.eof())
            break;
        delay.push_back(temp3);
    }

    amount_vertex=delay.size();
    RAT=new double [amount_vertex];
    AAT=new double [amount_vertex];
    slack=new double [amount_vertex];
    for (int i=0; i<amount_vertex; i++) {
        AAT[i]=slack[i]=0;
        RAT[i]=INT_MIN;
    }
    incidence_list=new list <Edge> [amount_vertex];
    anti_incidence_list=new list <Edge> [amount_vertex];

    while (1) {
        nets>>temp1;
        nets>>temp2;
        nets>>temp3;
        if(nets.eof())
            break;
        tmp_edge.vertex=temp2;
        tmp_edge.delay=temp3;
        incidence_list[temp1].push_back(tmp_edge);
        tmp_edge.vertex=temp1;
        tmp_edge.delay=temp3;
        anti_incidence_list[temp2].push_back(tmp_edge);
    }
   
    while (1) {
        aux>>temp1;
        aux>>temp3;
        if(aux.eof())
            break;
        RAT[temp1]=temp3;
        out_vertex.push_back(temp1);
    }
    for (int i=0; i<amount_vertex; i++) {
        if (anti_incidence_list[i].empty())
            in_vertex.push_back(i);
    } 
    nodes.close();
    nets.close();
    aux.close();
}

Circuit::~Circuit() {
    delete [] RAT;
    delete [] AAT;
    delete [] slack;
    delete [] incidence_list;
    delete [] anti_incidence_list;
    in_vertex.clear();
    out_vertex.clear();
    delay.clear();
}

void Circuit::AAT_calculation() {
    list<Edge>::iterator it;
    double AAT_temp;
    int color[amount_vertex];
    for (int i=0; i<amount_vertex; i++) {
        color[i]=-1;
    }
    int v;
    queue <int> Q;
    for (int i=0; i<in_vertex.size(); i++) {
        color[in_vertex[i]]=0;
        Q.push(in_vertex[i]);
    }
    while (!Q.empty()) {
        v=Q.front();
        Q.pop();
        AAT_temp=delay[v];
        if (anti_incidence_list[v].empty()) {
            AAT[v]=AAT_temp;
        }
        for (it=anti_incidence_list[v].begin(); it!=anti_incidence_list[v].end(); it++) {
             if (color[(*it).vertex]!=1) {
                    Q.push(v);
                    break;
                }       
            AAT_temp+=(*it).delay+AAT[(*it).vertex];
            if (AAT_temp>AAT[v])
               AAT[v]=AAT_temp;
            AAT_temp=delay[v];     
        }
        for (it=incidence_list[v].begin(); it!=incidence_list[v].end(); it++) {
            if (color[(*it).vertex]==-1) {
                color[(*it).vertex]=0;
                Q.push((*it).vertex);
            }
        }
        color[v]=1;
    }
}

void Circuit::RAT_calculation() {
    list<Edge>::iterator it;
    double RAT_temp;
    int color[amount_vertex];
    for (int i=0; i<amount_vertex; i++) {
        color[i]=-1;
    }
    int v;
    queue <int> Q;
    for (int i=0; i<out_vertex.size(); i++) {
        color[out_vertex[i]]=0;
        Q.push(out_vertex[i]);
    }
    while (!Q.empty()) {
        v=Q.front();
        Q.pop();
        for (it=incidence_list[v].begin(); it!=incidence_list[v].end(); it++) {
                if (color[(*it).vertex]!=1) {
                    Q.push(v);
                    break;
                }                    
                RAT_temp=RAT[(*it).vertex]-(*it).delay-delay[v];
                if (RAT_temp>RAT[v])
                    RAT[v]=RAT_temp;
        }

        for (it=anti_incidence_list[v].begin(); it!=anti_incidence_list[v].end(); it++) {
            if (color[(*it).vertex]==-1) {
                color[(*it).vertex]=0;
                Q.push((*it).vertex);
            }
        }
        color[v]=1;
    }
}

void Circuit::slack_calculation() {
    for (int i=0; i<amount_vertex; i++) {
        slack[i]=RAT[i]-AAT[i];
    }
}

void Circuit::print_result() {
    fstream slacks, result;
    slacks.open("example.slacks", fstream::out);
    result.open("example.result", fstream::out);
    int flag=0, count=0;;
    for (int i=0; i<amount_vertex; i++) {
        if (slack[i]<0) {
            count++;
            flag=1;
        }
        slacks<<slack[i]<<endl;
    }
    if (flag==0) {
        result<<0<<endl;
    } else {
        result<<count<<endl;
        for (int i=0; i<amount_vertex; i++) {
            if (slack[i]<0)
                result<<i<<" ";
        }
    }
   slacks.close();
   result.close();
}

long long TimeValue=0;

unsigned long long time_RDTSC()

{ union ticks

  { unsigned long long tx;

    struct dblword { long tl,th; } dw; 

  } t;

  asm("rdtsc\n": "=a"(t.dw.tl),"=d"(t.dw.th));

  return t.tx;

} 

void time_start() { TimeValue=time_RDTSC(); }

long long time_stop() { return time_RDTSC()-TimeValue; }

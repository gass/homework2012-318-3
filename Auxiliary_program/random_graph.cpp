#include "random_graph.h"
#include "../slacks_exception.h"

using namespace std;

int get_random_int(int supremum, int infinum) {
    return rand()%(supremum+1)+infinum;
}

double get_random_double(int supremum) {
    double temp;
    temp=(unsigned int)rand()%100;
    temp/=100;
    return rand()%supremum+temp;
}

void make_seed() {
    srand(time(0));
}

RandomGraph::RandomGraph(const char *filename) {
    fstream input;
    input.open(filename, fstream::in);
    if (!input) {
        throw DontOpenFileException(filename);
    }
    input>>amount_vertex;
    input>>amount_edge;

    delay=new double [amount_vertex];
    color=new int [amount_vertex];
    incidence_list=new list<Edge>[amount_vertex];
    for (int i=0; i<amount_vertex; i++)
        color[i]=0;
    cycle_state=-1;
    make_seed();    
}

bool RandomGraph::is_ok(int from, int to) const {
    if (from==to) return false; 
    if (incidence_list[from].empty()) return true;
    list <Edge>::iterator it;
    for (it=incidence_list[from].begin(); it!=incidence_list[from].end(); it++) {
        if ((*it).vertex==to)
            return false;
    }
    return true;
}

void RandomGraph::generate() {
    int from;
    Edge temp_edge;
    set <int> using_vertex;
    bool flag=false;
    int *count_vertex=new int [amount_vertex];
    int in_vertex=get_random_int(amount_vertex/4+1, 1);
    int out_vertex=get_random_int(amount_vertex/4+1, 1);
    int *out_count= new int [amount_vertex];
    int max_edge=0;
    amount_start_vertex=in_vertex;
    for (int i=1 ; i<=amount_vertex; i++) {
        max_edge+=abs(i-in_vertex);
    }
    max_edge-=out_vertex;
    if (amount_edge>max_edge) {
        cout<<"Too mane edges"<<endl;
        return;
    }
    do {
    for (int i=0; i<amount_vertex; i++) {
        incidence_list[i].clear();
    }
    for (int i=0; i<amount_vertex; i++) {
        if (i>=amount_vertex-out_vertex) {
            out_count[i]=amount_vertex-1;
        } else {
            out_count[i]=0;
        }
    }

    for (int i=0; i<amount_vertex; i++) {
        delay[i]=get_random_double(MAX_DELAY);        
    }
    
    for (int i=0; i<amount_vertex; i++) {
        if (i<in_vertex) { 
            count_vertex[i]=0;
        } else {
            count_vertex[i]=amount_vertex-1;
        }
    }
        for (int i=0; i<amount_edge; i++) {
            while (!flag) {
            if (out_count[from=get_random_int(amount_vertex-1, 0)]<amount_vertex-1) {
                if (count_vertex[temp_edge.vertex=get_random_int(amount_vertex-1,0)]>0) {
                    if (is_ok(temp_edge.vertex,from) && is_ok(from, temp_edge.vertex)) {
                        temp_edge.delay=get_random_double(MAX_DELAY);
                        incidence_list[from].push_back(temp_edge);
                        out_count[from]++;
                        count_vertex[temp_edge.vertex]--;
                        flag=true;
                    }
                }
            }
           }
           flag=false;
        }
          
   
    } while (!connected() && is_cycling());
    delete [] out_count;
    delete [] count_vertex;
}

RandomGraph::~RandomGraph() {
    delete [] color;
    delete [] delay;
    delete [] incidence_list;
};

void RandomGraph::print_result(const char *filenodes,
                               const char *filenets,
                               const char *filerat) const {
    fstream nodes, nets;
    list <Edge>::iterator it;
    nodes.open(filenodes, fstream::out);
    if (!nodes) {
        throw DontOpenFileException(".nodes");
    }
    nets.open(filenets, fstream::out);
    if (!nets) {
        throw DontOpenFileException(".nets");
    }
    for (int i=0; i<amount_vertex; i++) {
        nodes<<delay[i]<<endl;
    }
    for (int i=0; i<amount_vertex; i++) {
        for (it=incidence_list[i].begin(); it!=incidence_list[i].end(); it++) {
            nets<<i<<" "<<(*it).vertex<<" "<<(*it).delay<<endl;
        }
    }
    fstream rat;
    rat.open(filerat, fstream::out); 
    for (int i=0; i<amount_vertex; i++) {
        if (incidence_list[i].empty()) {
            rat<<i<<" "<<get_random_int((int)MAX_DELAY/2*amount_vertex, 0)+get_random_double(1)<<endl;
        }
    }
    nodes.close();
    nets.close();
    rat.close();
}

bool RandomGraph::connected() {
    list<Edge>::iterator it;
    int cl[amount_vertex];
    for (int i=0; i<amount_vertex; i++) {
        cl[i]=-1;
    }
    int v;
    queue <int> Q;
    for (int i=0; i<amount_start_vertex; i++) {
        cl[i]=0;
        Q.push(i);
    }
   while (!Q.empty()) {
        v=Q.front();
        Q.pop();
         for (it=incidence_list[v].begin(); it!=incidence_list[v].end(); it++) {
            if (cl[(*it).vertex]==-1) {
                cl[(*it).vertex]=0;
                Q.push((*it).vertex);
            }
        }
        cl[v]=1;
    }
    for (int i=0; i<amount_vertex; i++) {
        if (cl[i]!=1) return false;
    }
    return true;
}
bool RandomGraph::dfs_visit(int s) {
    color[s]=1;
    list <Edge>::iterator it;
    for (it=incidence_list[s].begin(); it!=incidence_list[s].end(); it++) {
        if (color[(*it).vertex]==0) {
           if (dfs_visit((*it).vertex)) return true;
        } else {
            if (color[(*it).vertex]==1) {
                cycle_state=(*it).vertex;
                return true;
            }
        }
    }
    color[s]=2;
    return false;
}

bool RandomGraph::is_cycling() {
    for (int i=0; i<amount_vertex; i++) {
            if (dfs_visit(i))
                break;   
    }
    if (cycle_state==-1)
        return false;
    else
        return true;

}


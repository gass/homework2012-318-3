﻿#ifndef RANDOM_GRAPH_H
#define RANDOM_GRAPH_H

#include <fstream>
#include <list>
#include <vector>
#include <iostream>
#include <set>

#include <ctime>
#include <cstdlib>
#include <cstring>

#include "../slacks.h"


using namespace std;
/**
 * Макимальная задержка на вершине.
*/
enum {MAX_DELAY = 10};
/**
 * Данный класс при помощи своих методов и данных генерирует и хранит случайный  
 * ориентированный ациклический граф. Количество вершин и ребер в графе считываются из файла.
 * @author Антон Гой
 * @throws DontOpenFileException
*/

class RandomGraph {
    public:
/**
 * Конструктор преобразования. Выделение памяти под данные, необходимы при генерации графа.
 * @param Имя файла с количеством вершин и ребер
 * @throws DontOpenFileException
*/
        RandomGraph(const char *);
/**
 * Деструктор. Освобождает память.
*/
        ~RandomGraph();
/**
 * Данный метод генерирует граф. Генерация происходит в цикле.
 * Перед циклом случайно выбирается количество входных и выходных вершин.
 * Случайным образом с помощью функций {@link int get_random_int()} и 
 * {@link double get_random_double()} генерируется начало, конец ребра и задержка на нем.
 * Если ребро можно разместить в данном месте, т.е. оно не кратное, не петля, не образует простой цикл, то
 * генерируется следующее ребро, иначе ребро создается заново. Когда все ребра сгенерированы, граф проверяется 
 * на цикличность и связность, если эти требования не выполняются, то граф создается с нуля.
 * @see double get_random_double()
 * @see int get_random_int()
 * @see bool is_ok()
 * @see bool is_cycling()
 * @see bool connected()
*/
        void generate();
/**
 * Метод класса, который проверяет является ли ребро
 * петлей и проверяет есть ли такое ребро в графе.
 * @param Начало ребра
 * @param Конец ребра
 * @return true - если такого ребра нет. false - если такое ребро есть или если это петля
*/
        bool is_ok(int, int) const;
/**
 * Метод класса, который печает результирующий случайный граф в три файла: 
 * .nodes, .nets, .aux.
 * @param имя файла .nodes
 * @param имя файла .nets
 * @param имя файла .aux
*/
        void print_result(const char *, const char *, const char *) const;
/**
 * Данный метод проверяет является ли граф цикличным. Для этого он вызывает 
 * для каждой вершины функцию {@link bool dfs_visit()}, которая рекурсивно обходит граф в глубину,
 * помечая вершины. Если поиск заходит в уже пройденную вершину, значит цикл есть и данный метод
 * фиксирует его.
 * @return true - если цикл был. false - если нет ни одного цикла 
 * @see bool dfs_visit(int)
*/
        bool is_cycling();
/**
 * Проверяет созданый граф на связность. Для распознования связности
 * используется поиск в ширину, пройденные вершины помечаются. В конце, если какие-то вершины 
 * остались не помеченными, то граф не связный.
 * @return true - если граф связный, false - иначе
*/
        bool connected();
/**
 * Рекурсивный поиск в глубину с началом из данной вершины. При обходе вершины помечаются.
 * Если обход вошел в помеченную вершину.
 * @param вершина начало поиска
*/
        bool dfs_visit(int);
    private:
        list <Edge> *incidence_list;
        int amount_vertex;
        int amount_edge;
        int *color;
        int cycle_state;
        int amount_start_vertex;
        double *delay;
        vector <pair<int, double> > RAT_out;
};

/**
 * Функция генерирует случайное целое число
 * @param максимальное значение случайного числа
 * @return случайное число
*/
int get_random_int(int);
/**
 * Функция генерирует случайное число с плавающей точкой
 * @param максимальное значение случайного числа
 * @return случайное число
*/
double get_random_double(int);
/**
 * Задает семя для случайных чисел.
*/
void make_seed();

#endif //RANDOM_GRAPH_H

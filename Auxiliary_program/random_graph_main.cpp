#include "random_graph.h"
#include "../slacks_exception.h"
#include "../slacks.h"

int main() {
    try {
        RandomGraph G("../example.in");
        G.generate();
        G.print();
        G.print_result("../example.nodes", "../example.nets", "../example.aux");
    }
    catch(GeneralException &excep) { excep.what(); }
    return 0;
}

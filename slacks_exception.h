#ifndef SLACKS_EXCEPTION_H
#define SLACKS_EXCEPTION_H

#include <iostream>
#include <string>

using namespace std;

class GeneralException {
    public:
       virtual void what()=0;
};

class NegativeWeightException: public GeneralException {
    public:
        void what();
};

class DontOpenFileException: public GeneralException {
    public:
        DontOpenFileException(const char *);
        void what();
    private:
        string status;
};

#endif //SLACKS_EXCEPTION_H

#include "slacks_exception.h"

using namespace std;

DontOpenFileException::DontOpenFileException(const char *_status) {
    status.append(_status);
}

void DontOpenFileException::what() {
    cout<<"Can't open the file "<<status<<endl;
}

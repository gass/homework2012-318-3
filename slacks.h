﻿#ifndef SLACKS_H
#define SLACKS_H

#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <queue>

#include <ctime>
#include <cstdio>
#include <climits>
#include <sys/time.h>

void time_start();

long long time_stop();

using namespace std;
/**
 * Структура используемая для хранения информации о ребере, 
 * а именно Edge содержит в себе конечную вершину ребра - <tt>vertex</tt>, 
 * и задержка на этом ребре -  <tt>delay</tt>.
*/
struct Edge {
/**
 * Конструктор по умолчанию. Устанавливает <tt>vertex=-1</tt>, a <tt>delay=0</tt>.
*/
        Edge();
/**
 * Конструктор с двумя параметрами <tt>int</tt>, которые соотвественно 
 * присваиваются <tt>vertex</tt> и <tt>delay</tt>. 
*/
        Edge(int, int);
/**
 * Хранит конец ребра.
*/
        int vertex;
/**
 * Хранит задержку на ребре.
*/
        double delay;    
};

/**
 * Данный класс хранит сам граф и все данные необходимые для вычисления 
 * <i>AAT</i>, <i>RAT</i> и <i>slack</i>. Методы этого класса совершают полное вычисление
 * выше перечисленных параметров графа. Для внутреннего представления графа используется список смежности
 * <tt>incidence_list</tt>, также в классе Circuit храниться так называемый <tt>anti_incidence_list</tt>, в
 * котором реализуется граф идентичный исходному, но с ребрами напрвленными в противоположные стороны, это сделано для более
 * удобного вычисления <i>AAT</i>, <i>RAT</i> и <i>slack</i>.
 * @author Антон Гой
 * @throws DontOpenFileException
*/

class Circuit {
    public:
/** 
 * Конструктор с тремя параметрами. Инициализирует объект класса Circuit.
 * @param Имя файла .nodes
 * @param Имя файла .nets
 * @param Имя файла .aux
*/
        Circuit(const char *, const char *, const char *);
/**
 * Деструктор. Уничтожает всю диномически выделенную память.
*/
        ~Circuit();
/**
 * Метод класса вычисляющая <i>AAT</i> для каждой вершины. Для вычисления <i>AAT</i>
 * используется поиск в ширину. Результатом работы метода является заполненый массив
 * <i>AAT</i>.
*/
        void AAT_calculation();
/**
 * Метод класса вычисляющая <i>RAT</i> для каждой вершины. Для вычисления <i>AAT</i>
 * используется поиск в ширину, но обход 
 * ведется не по исходному графу, а по "обратному" графу (<tt>anti_incidence_list</tt>).
 * Результатом работы метода является заполненый массив
 * <i>RAT</i>.
*/
        void RAT_calculation();
/**
 * Данный метод вычисляет slack для каждой вершины. Результат работы - массив <i>slack</i>.
*/
        void slack_calculation();
/**
 * Метод класса печатающий результат работы методов класса 
 * {@link void AAT_calculation()}, {@link void RAT_calculation()},
 * {@link void slack_calculation()}. 
 * <p>Результат печатается в файлы
 * .slacks, .result. В .slacks выводится на iой строке
 * информация о slack[i-1]. В .result печатается количество
 * вершин с отрицательным slack, и перечисляются эти вершины.
*/
        void print_result();
    private:
        list <Edge> *incidence_list;
        list <Edge> *anti_incidence_list;
        int amount_vertex;
	    vector <double> delay;
        vector <int> out_vertex;
        vector <int> in_vertex;
        double *RAT;
        double *AAT;
        double *slack;
};

#endif //SLACKS_H

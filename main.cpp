#include "slacks.h"
#include "slacks_exception.h"

using namespace std;



int main (int argc, char *argv[]) {
    time_start();
    Circuit G("example.nodes", "example.nets", "example.aux");
    G.AAT_calculation();
    G.RAT_calculation();
    G.slack_calculation();
    G.print_result();
    printf("Time: %Ld микросекунд\n", time_stop()/2500);
    return 0;
}
